package pl.akademia.company.domain;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import pl.akademia.company.domain.Employee;

/*
    do wykonywania zapytań SQL do Bazy danych
    ODPOWIEDZIALNOŚC TEJ KLASY TO ZAPYTANIA DO BAZY DANYCH
 */
// http://localhost:8080/job?job=CLERK
public interface EmployeeRepository extends
        CrudRepository<Employee,Integer> {
    // findAll()

    // SPOSÓB 1 ?1 oznacza 1 argument, nastepny argument ?2
    @Query("SELECT e FROM Employee e WHERE e.job=?1")
    Iterable<Employee> findEmployeesByJob(String job);

    // SPOSÓB 2 , jeśli nazwiemy zgodnie z konwencją
    // to @Query zrobi z nas Hibernate
    // to nie jest uniwersalny sposób, działa przy prostych zapytaniach
    // MAGIA
    Iterable<Employee> findByJob(String job);

    // SPOSÓB 3
    @Query(value = "SELECT * FROM EMP WHERE job=?1",nativeQuery = true)
    Iterable<Employee> findEMP(String job);


}
