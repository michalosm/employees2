package pl.akademia.company.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "DEPT")
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DEPTNO")
    private Integer id;

    @Column(name = "DNAME")
    private String name;

    @Column(name = "LOC")
    private String loc;





}
