package pl.akademia.company.domain;

import org.springframework.data.repository.CrudRepository;
import pl.akademia.company.domain.Department;


public interface DepartmentRepository extends CrudRepository<Department, Integer> {

}
