package pl.akademia.company.api;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.akademia.company.domain.Employee;
import pl.akademia.company.domain.EmployeeRepository;


@RestController
@RequestMapping("/")
public class EmployeeController  {

    private EmployeeRepository employeeRepository;

    public EmployeeController(EmployeeRepository employeeRepository){
        this.employeeRepository = employeeRepository;
    }

    @GetMapping("employees")
    public Iterable<Employee> index(){
        // SELECT * FROM EMP;
        return employeeRepository.findAll();
    }

    @GetMapping("job")
    public Iterable<Employee> findByJob(@RequestParam(required = false) String job){
        if ( job!=null) {
            return employeeRepository.
                    findEMP(job.toUpperCase());
        }
        else{
            return employeeRepository.findAll();
        }
    }

}
