package pl.akademia.company.api;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.akademia.company.domain.Department;
import pl.akademia.company.domain.DepartmentRepository;


@RequestMapping("/departments")
@RestController
public class DepartmentController {

    private DepartmentRepository departmentRepository;

    public DepartmentController(DepartmentRepository departmentRepository){
        this.departmentRepository = departmentRepository;
    }

    @GetMapping("")
    public Iterable<Department> getAllDepartments(){
        return departmentRepository.findAll();
    }


}
